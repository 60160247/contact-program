
import java.util.ArrayList;

public class People {

    static ArrayList person = new ArrayList<People>();
    private String name;
    private String surname;
    private String username;
    private String password;
    private String tel;
    private double weight;
    private double height;

    public People(String name, String surname, String username, String password, String tel, double weight, double height) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.tel = tel;
        this.weight = weight;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getTel() {
        return tel;
    }

    public double getWeight() {
        return weight;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "People{" + "name=" + name + ", surname=" + surname + ", username=" + username + ", password=" + password + ", tel=" + tel + ", weight=" + weight + ", height=" + height + '}';
    }

    public static void load() {
        person.add(new People("admin", "admin", "admin", "admin", "0888888888", 50, 180));
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setHeight(double height) {
        this.height = height;
    }

}
