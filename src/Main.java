
import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Peppa Point
 */
public class Main {

    static ArrayList<People> person = People.person;
    static Scanner ip = new Scanner(System.in);
    static String username;
    static People user;

    public static void main(String[] args) {
        for (;;) {
            People.load();
            if (Homepage() == 1) {
                for (;;) {
                    if (Login() == true) {
                        for (;;) {
                            boolean flag = false;
                            loadUserDeatil();
                            switch (Menu()) {
                                case 1:
                                    AddUser();
                                    break;
                                case 2:
                                    EditUser();
                                    break;
                                case 3:
                                    ShowUser();
                                    break;
                                default:
                                    flag = true;
                                    break;
                            }
                            if (flag == true) {
                                break;
                            }
                        }
                    } else {
                        Exit();
                        break;
                    }
                }
            } else {
                break;
            }
        }

    }

    private static int Homepage() {
        showHomePage();
        if (selectFuctionHomepage() == 1) {
            return 1;
        } else {
            return 2;
        }
    }

    private static int selectFuctionHomepage() {
        for (;;) {
            String num = ip.next();
            switch (num) {
                case "1":
                    return 1;
                case "2":
                    System.out.println("Exit");
                    return 2;
                default:
                    showSelectFuctionHomepageError();
                    break;
            }
        }

    }

    private static void showHomePage() {
        System.out.println("Homepage");
        System.out.println("1. Login");
        System.out.println("2. Exit");
        System.out.print("Please input number (1,2) : ");
    }

    private static void Exit() {
        System.out.println("Exit");
    }

    private static void showSelectFuctionHomepageError() {
        System.out.print("Error : Please input number 1 or 2 : ");
    }

    private static boolean Login() {
        for (;;) {
            showLogin();
            System.out.print("Username : ");
            username = ip.next();
            System.out.print("Password : ");
            String password = ip.next();
            if (checkInputLogin(username, password) == true) {
                return true;
            } else {
                showLoginError();
                if (askBack() == true) {
                    return false;
                }

            }
        }

    }

    private static void showLogin() {
        System.out.println("Login");
    }

    private static boolean checkInputLogin(String username, String password) {
        for (People a : person) {
            return a.getUsername().equals(username) && a.getPassword().equals(password);
        }
        return false;
    }

    private static void showLoginError() {
        System.out.println("Error : Username or Password is incorrect");
    }

    private static int Menu() {
        showMenu();
        switch (selectFuctionMenu()) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            default:
                return 4;
        }
    }

    private static void showMenu() {
        System.out.println("Main Menu");
        System.out.println("1. Add User");
        System.out.println("2. Edit User");
        System.out.println("3. Show User");
        System.out.println("4. Logout");
        System.out.print("Please input number 1-4 : ");
    }

    private static boolean askBack() {
        showAskBack();
        if (inputAskBack() == true) {
            return true;
        } else {
            return false;
        }

    }

    private static boolean inputAskBack() {
        for (;;) {
            String num = ip.next();
            if (num.equals("0")) {
                return true;
            } else if (num.equals("1")) {
                return false;
            } else {
                showInputAskBackError();
            }
        }

    }

    private static void showAskBack() {
        System.out.println("Do you want to exit program ? ");
        System.out.println("0. Yes");
        System.out.println("1. No");
        System.out.print("Please input number 0 or 1 : ");
    }

    private static void showInputAskBackError() {
        System.out.print("Error : Please input number 0 or 1 : ");
    }

    private static int selectFuctionMenu() {
        for (;;) {
            String num = ip.next();
            if (num.equals("1")) {
                return 1;
            } else if (num.equals("2")) {
                return 2;
            } else if (num.equals("3")) {
                return 3;
            } else if (num.equals("4")) {
                return 4;
            } else {
                showSelectFuctionMenuError();
            }
        }
    }

    private static void showSelectFuctionMenuError() {
        System.out.print("Error : Number must be 1-4 : ");
    }

    private static void AddUser() {
        showAddUser();
        String name = inputName();
        String surname = inputSurname();
        String usernameadd = inputUsername();
        String password = inputPassword();
        String tel = inputTel();
        double weight = inputWeight();
        double height = inputHeight();
        addUser(name, surname, usernameadd, password, tel, weight, height);
        showAddUserSuccess();

    }

    private static double inputHeight() {
        double height = checkHeight();
        return height;
    }

    private static double inputWeight() {
        double weight = checkWeight();
        return weight;
    }

    private static String inputPassword() {
        System.out.print("Pasword : ");
        String password = ip.next();
        return password;
    }

    private static String inputSurname() {
        System.out.print("Surname : ");
        String surname = ip.next();
        return surname;
    }

    private static String inputName() {
        System.out.print("Name : ");
        String name = ip.next();
        return name;
    }

    private static void showAddUserSuccess() {
        System.out.println("Add successful");
    }

    private static void addUser(String name, String surname, String username, String password, String tel, double weight, double height) {
        People.person.add(new People(name, surname, username, password, tel, weight, height));
    }

    private static double checkHeight() {
        for (;;) {
            boolean flag = true;
            System.out.print("Height : ");
            String height = ip.next();
            try {
                double num = Double.parseDouble(height);
            } catch (NumberFormatException e) {
                flag = false;
            }

            if (flag == false) {
                printErrorHeight();
            } else {
                return Double.parseDouble(height);
            }
        }

    }

    private static double checkWeight() {
        for (;;) {
            boolean flag = true;
            System.out.print("Weight : ");
            String weight = ip.next();
            try {
                double num = Double.parseDouble(weight);
            } catch (NumberFormatException e) {
                flag = false;
            }

            if (flag == false) {
                printErrorWeight();
            } else {
                return Double.parseDouble(weight);
            }
        }

    }

    private static void printErrorHeight() {
        System.out.println("Error : height must be number!!!");
    }

    private static void printErrorWeight() {
        System.out.println("Error : weight must be number!!!");
    }

    private static String inputTel() {
        for (;;) {
            boolean flag = true;
            System.out.print("Tel : ");
            String tel = ip.next();
            for (int i = 0; i < tel.length(); i++) {
                try {
                    int n = Integer.parseInt(tel.substring(i));
                } catch (NumberFormatException e) {
                    flag = false;
                }
            }
            if (flag == false) {
                showAddTelError();
            } else {
                return tel;
            }
        }

    }

    private static void showAddTelError() {
        System.out.println("Error : Tel must be number!!!");
    }

    private static void showAddUser() {
        System.out.println("Add User");
    }

    private static void EditUser() {
        for (;;) {
            boolean flag = false;
            showEditUser();
            for (;;) {
                if (selectFunctionEditUser() == 1) {
                    EditUserDetail();
                    break;
                } else {
                    flag = true;
                    break;
                }
            }
            if (flag == true) {
                break;
            }
        }

    }

    private static void ShowUser() {
        for (;;) {
            boolean flag = false;
            showShowUserMenu();
            switch (selectShowUserFuction()) {
                case 1:
                    SearchByUsername();
                    break;
                case 2:
                    ShowAllUser();
                    break;
                default:
                    flag = true;
                    break;
            }
            if (flag == true) {
                break;
            }
        }

    }

    private static String inputUsername() {
        for (;;) {
            boolean flag = false;
            System.out.print("Username : ");
            String usernameadd = ip.next();
            for (People a : person) {
                if (a.getUsername().equals(usernameadd)) {
                    flag = true;
                    break;
                }
            }
            if (flag == true) {
                boolean back = false;
                showAddUsernameError();
                
            } else {
                return usernameadd;
            }
        }
    }

    private static void showAddUsernameError() {
        System.out.println("Error : username is exist!!!");
    }

    private static void showEditUser() {
        System.out.println("Edit User");
        System.out.println("1. Edit User Detail");
        System.out.println("2. Exit");
        System.out.print("Please input number 1-2 : ");
    }

    private static int selectFunctionEditUser() {
        for (;;) {
            String num = ip.next();
            if (num.equals("1")) {
                return 1;
            } else if (num.equals("2")) {
                return 2;
            } else {
                showSelectFunctionEditUserError();
            }
        }
    }

    private static void showSelectFunctionEditUserError() {
        System.out.print("Error : Please input number (1-2) : ");
    }

    private static void EditUserDetail() {
        showEditUserDetail();
        editName();
        editSurname();
        editPassword();
        editTel();
        editWeight();
        editHeight();
    }

    private static void showEditUserDetail() {
        System.out.println("Edit User Detail");
    }

    private static void editName() {
        System.out.print("New name : ");
        String name = ip.next();
        user.setName(name);
    }

    private static void loadUserDeatil() {
        for (People a : person) {
            if (username.equals(a.getUsername())) {
                user = a;
                break;
            }
        }
    }

    private static void editSurname() {
        System.out.print("New surname : ");
        String surname = ip.next();
        user.setSurname(surname);
    }

    private static void editPassword() {
        System.out.print("New Password : ");
        String password = ip.next();
        user.setPassword(password);
    }

    private static void editTel() {
        System.out.print("New tel : ");
        for (;;) {
            boolean flag = true;
            String tel = ip.next();
            for (int i = 0; i < tel.length(); i++) {
                try {
                    int num = Integer.parseInt(tel.substring(i));
                } catch (NumberFormatException e) {
                    flag = false;
                    break;
                }
            }
            if (flag == false) {
                showEditTelError();
            } else {
                user.setTel(tel);
                break;
            }
        }

    }

    private static void showEditTelError() {
        System.out.print("Error : Tel must be number!!! : ");

    }

    private static void editWeight() {
        System.out.print("New weight : ");
        for (;;) {
            boolean flag = true;
            String weight = ip.next();
            try {
                double num = Double.parseDouble(weight);
            } catch (NumberFormatException e) {
                flag = false;
            }
            if (flag == false) {
                showEditWeightError();
            } else {
                user.setWeight(Double.parseDouble(weight));
                break;
            }
        }
    }

    private static void editHeight() {
        System.out.print("New height : ");
        for (;;) {
            boolean flag = true;
            String height = ip.next();
            try {
                double num = Double.parseDouble(height);
            } catch (NumberFormatException e) {
                flag = false;
            }
            if (flag == false) {
                showEditHeightError();
            } else {
                user.setWeight(Double.parseDouble(height));
                break;
            }
        }
    }

    private static void showEditWeightError() {
        System.out.print("Error : Weight must be number!!! : ");
    }

    private static void showEditHeightError() {
        System.out.print("Error : Height must be number!!! : ");
    }

    private static void showShowUserMenu() {
        System.out.println("Show User");
        System.out.println("1. Search by username");
        System.out.println("2. Show all user");
        System.out.println("3. Back to main menu");
        System.out.print("Please input number 1-3 : ");
    }

    private static int selectShowUserFuction() {
        for (;;) {
            String num = ip.next();
            switch (num) {
                case "1":
                    return 1;
                case "2":
                    return 2;
                case "3":
                    return 3;
                default:
                    showSelectShowUserFuctionError();
                    break;
            }
        }
    }

    private static void showSelectShowUserFuctionError() {
        System.out.print("Error : Please input number 1-3 : ");
    }

    private static void SearchByUsername() {
        for (;;) {
            System.out.print("Please input username : ");
            String usernamesearch = ip.next();
            for (People a : person) {
                if (a.getUsername().equals(usernamesearch)) {
                    System.out.println("Name : " + a.getName());
                    System.out.println("Surname : " + a.getSurname());
                    System.out.println("Username : " + a.getUsername());
                    System.out.println("Password : " + a.getPassword());
                    System.out.println("Tel : " + a.getTel());
                    System.out.println("Weight : " + a.getWeight());
                    System.out.println("Height : " + a.getHeight());
                    break;
                } else {
                    showUserNotFound();
                    break;
                }
            }
            break;
        }
    }

    private static void ShowAllUser() {
        for (People a : person) {
            System.out.println("Name : " + a.getName());
            System.out.println("Surname : " + a.getSurname());
            System.out.println("Username : " + a.getUsername());
            System.out.println("Password : " + a.getPassword());
            System.out.println("Tel : " + a.getTel());
            System.out.println("Weight : " + a.getWeight());
            System.out.println("Height : " + a.getHeight());
            System.out.println("-------------------------");
        }
    }

    private static void showUserNotFound() {
        System.out.println("User not found!!!");
    }

}
